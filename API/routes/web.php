<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view("welcome");
});

Route::match(['get','post'], 'rest/getCategoryList', 'API\ManageCategoryController@getCategoryList');
Route::match(['get','post'], 'rest/createCategory', 'API\ManageCategoryController@createCategory');
Route::match(['get','post'], 'rest/updateCategory', 'API\ManageCategoryController@updateCategory');
Route::match(['get','post'], 'rest/deleteCategory', 'API\ManageCategoryController@deleteCategory');
Route::match(['get','post'], 'rest/getCategoryDetails', 'API\ManageCategoryController@getCategoryDetails');

Route::match(['get','post'], 'rest/getAllUsers', 'API\UsersAPIController@getUserList');
Route::match(['get','post'], 'rest/registerUser', 'API\UsersAPIController@createUser');
Route::match(['get','post'], 'rest/updateProfileDetails', 'API\UsersAPIController@updateProfileDetails');
Route::match(['get','post'], 'rest/deleteUser', 'API\UsersAPIController@deleteUser');
Route::match(['get','post'], 'rest/getProfileDetails', 'API\UsersAPIController@getProfileDetails');
Route::match(['get','post'], 'rest/createAdmin', 'API\UsersAPIController@createAdmin');
Route::match(['get','post'], 'rest/loginAdmin', 'API\UsersAPIController@loginAdmin');


Route::match(['get','post'], 'rest/createProduct', 'API\ManageProductController@createProduct');
Route::match(['get','post'], 'rest/getProductList', 'API\ManageProductController@getProductList');
Route::match(['get','post'], 'rest/updateProductDetails', 'API\ManageProductController@updateProductDetails');
Route::match(['get','post'], 'rest/getProductDetails', 'API\ManageProductController@getProductDetails');
Route::match(['get','post'], 'rest/deleteProduct', 'API\ManageProductController@deleteProduct');

Route::match(['get','post'], 'rest/sendSMS', 'API\ManageSMSController@sendSMS');
Route::match(['get','post'], 'rest/sendMarketingEmail', 'API\ManageEmails@sendEmail');

Route::match(['get','post'], 'rest/addToCart', 'API\ManageCart@addToCart');
Route::match(['get','post'], 'rest/removeFromCart', 'API\ManageCart@removeFromCart');
Route::match(['get','post'], 'rest/getCartList', 'API\ManageCart@getCartList');

Route::match(['get','post'], 'rest/getAddressList', 'API\ManageAddressController@getAddressList');

Route::match(['get','post'], 'rest/getServiceAreaList', 'API\ManageShippingDetails@getServiceAreaList');
Route::match(['get','post'], 'rest/createServiceArea', 'API\ManageShippingDetails@createServiceArea');
Route::match(['get','post'], 'rest/deleteServiceArea', 'API\ManageShippingDetails@deleteServiceArea');
Route::match(['get','post'], 'rest/updateServiceArea', 'API\ManageShippingDetails@updateServiceArea');

Route::match(['get','post'], 'rest/getImageList', 'API\ManageGalleryController@getImageList');
Route::match(['get','post'], 'rest/createImage', 'API\ManageGalleryController@createImage');
Route::match(['get','post'], 'rest/deleteImage', 'API\ManageGalleryController@deleteImage');

Route::match(['get','post'], 'rest/sendDummyEmail', 'API\ManageEmails@sendDummyEmail');

Route::match(['get','post'], 'rest/getDashboardData', 'API\ManageDashboard@getDashboardData');
Route::match(['get','post'], 'rest/getMyIP', 'API\ManageDashboard@getIPAddress');


Route::match(['get','post'], 'rest/getVisitorsData', 'API\ManageVisitors@getVisitorsData');
Route::match(['get','post'], 'rest/createVisitor', 'API\ManageVisitors@createVisitor');


