<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingCharge extends Model
{
    //
    protected $fillable = [
        'locality', 'city', 'state', 'shipping_method', 'average_time',
        'country', 'pin_code', 'shipping_charge', 'minimum_order_amount'
    ];
}
