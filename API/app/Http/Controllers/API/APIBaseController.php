<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class APIBaseController extends Controller
{
    //
    public function sendResponse($result, $message)
    {
    	$response = [
            'code' => 200,
            'success' => true,
            'message' => $message,
            'data'    => $result,
        ];
        if ($result==null){
            $response = [
                'code' => 200,
                'success' => true,
                'message' => $message,
            ];
        }


        return response()->json($response, 200);
    }

    public function sendErrorResponse($code, $result = [], $message)
    {
    	$response = [
            'code' => $code,
            'success' => false,
            'message' => $message,
        ];

        if(!empty($result)){
            $response['data'] = $result;
        }

        return response()->json($response, 200);
    }

    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$response = [
            'code' => 400,
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }

    public function checkAuthToken($request){
        if(is_null($request['authToken'])){
            $response = [
                'code' => 407,
                'success' => false,
                'message' => "AuthToken required.",
            ];
            return $response;
        }
        else if(($request['authToken']) != env("APP_TOKEN", "")){
            $response = [
                'code' => 408,
                'success' => false,
                'message' => "AuthToken mismatch.",
            ];
            return $response;
        }
        else{
            $response = [
                'code' => 200,
                'success' => false,
                'message' => "AuthToken mismatch.",
            ];
            return $response;
        }
    }
}
