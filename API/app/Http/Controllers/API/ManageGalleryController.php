<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

use Validator;
use App\Gallery;
use App\Product;
use Illuminate\Support\Facades\DB;
use File;

class ManageGalleryController extends APIBaseController
{
    /*
        API Name        : getUserList
        Method          : GET/POST
        Input Parameter : {authToken}
        Output Parameter: {category_id, category_name, category_image, category_status}

    */
    public function getImageList(Request $request)
    {
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $gallery = Gallery::all();
            return $this->sendResponse($gallery, 'Success');
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }
    /*
        API Name        : createProduct
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, name, email, password, phone(O), profile_photo(O)}
        Output Parameter: {id, name, email, phone, profile_photo}

    */
    public function createImage(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();
            $validator = Validator::make($input, [
                'file_name'         => ['required', 'string', 'max:255','unique:galleries'],
                'original_file'     => ['required', 'image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
            ]);


            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            if($request->hasFile('original_file')) {

                $filenamewithextension = $request->file('original_file')->getClientOriginalName();
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                $extension = $request->file('original_file')->getClientOriginalExtension();
                $filenametostore = $filename.'_'.uniqid().'.'.$extension;
                $filenametostoreThumbnail = "thumb-".$filename.'_'.uniqid().'.'.$extension;
                $filenametostore =  preg_replace('/\s+/', '_', $filenametostore);
                $filenametostoreThumbnail =  preg_replace('/\s+/', '_', $filenametostoreThumbnail);

                $fullFilePath = $request->file('original_file')->store(
                    'images/galleryImages/originalImage', 'publicDriver'
                );

                $fileNameDB = env('BASE_URL').'/public'.'/'.$fullFilePath;
                $this->generateThumbnail($fileNameDB, $filenametostoreThumbnail);
                $fullFilePathThumb = 'images/galleryImages/thumbImage/'.$filenametostoreThumbnail;
                $newProduct = Gallery::create($input);
                $productInfo = Gallery::find($newProduct->id);
                $productInfo->thumbnail_image = env('BASE_URL').'/public'.'/'.$fullFilePathThumb;
                $productInfo->original_file = $fileNameDB;
                $productInfo->file_type = $request->file('original_file')->getClientMimeType();
                $productInfo->save();
                return $this->sendResponse($productInfo->toArray(), 'Image created successfully.');
            }

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    public function generateThumbnail($img, $fileName, $width = 150 ,$quality = 90){
        $originalImage = imagecreatefromstring(file_get_contents($img));

        $imageWidth = imagesx($originalImage);
        $imageHeight = imagesy($originalImage);

        $height = ($width * $imageHeight) / $imageWidth;
        $outputImage = imagecreatetruecolor($width, $height);

        $fullFilePath = public_path('images/galleryImages/thumbImage/').$fileName;
        if(!File::exists(public_path('images/galleryImages/thumbImage/'))){
            mkdir(public_path('images/galleryImages/thumbImage/'),0755);
        }

        if(imagecopyresampled($outputImage, $originalImage, 0,0,0,0, $width, $height,
                            imagesx($originalImage), imagesy($originalImage))){

                                imagejpeg($outputImage, $fullFilePath, $quality);
                                return (realpath($fullFilePath));

        }
        return null;
    }

    /*
        API Name        : deleteUser
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, user_id}
        Output Parameter: {}

    */
    public function deleteImage(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();

            $validator = Validator::make($input, [
                'image_id'   => ['required', 'exists:galleries,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $currentProduct = Gallery::find($request['image_id']);
            $imageFileSlice = explode("/", $currentProduct->thumbnail_image);
            $imageFile = end($imageFileSlice);
            if(\File::exists(public_path('images/galleryImages/thumbImage/'.$imageFile))){
                \File::delete(public_path('images/galleryImages/thumbImage/'.$imageFile));
            }
            $originalImageFileSlice = explode("/", $currentProduct->original_file);
            $originalImageFile = end($originalImageFileSlice);
            if(\File::exists(public_path('images/galleryImages/originalImage/'.$originalImageFile))){
                \File::delete(public_path('images/galleryImages/originalImage/'.$originalImageFile));
            }
            $currentProduct->delete();
            return $this->sendResponse(null, 'Image deleted successfully.');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    /*
        API Name        : getCategoryDetails
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, category_id}
        Output Parameter: {id, category_name, category_image, category_status}

    */
    public function getProductDetails(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();

            $validator = Validator::make($input, [
                'product_id'   => ['required', 'exists:products,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $currentProduct = Product::find($request['product_id']);
            return $this->sendResponse($currentProduct, 'Success');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

}
