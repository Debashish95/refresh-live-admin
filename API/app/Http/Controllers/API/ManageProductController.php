<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

use Validator;
use App\Category;
use App\Product;
use Illuminate\Support\Facades\DB;
use File;

class ManageProductController extends APIBaseController
{
    /*
        API Name        : getUserList
        Method          : GET/POST
        Input Parameter : {authToken}
        Output Parameter: {category_id, category_name, category_image, category_status}

    */
    public function getProductList(Request $request)
    {
        $productList = new Product();
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            if(is_null($request['offset']) || $request['offset'] == 1){
                $offset = 0;
              }
              else{
                $offset = ($request['offset'] - 1) * $request['limit'];
              }
              if(is_null($request['limit'])){
                $limit = 10;
              }
              else{
                $limit = $request['limit'];
              }

              $orderBy = $request['orderby'];
              $orderByField = 'updated_at';
            switch($orderBy){
                case 'priceAsc':
                    $orderBy = 'asc';
                    $orderByField = 'product_price';
                    break;
                case 'priceDesc':
                    $orderBy = 'desc';
                    $orderByField = 'product_price';
                    break;
                case 'recent':
                    $orderBy = 'desc';
                    $orderByField = 'updated_at';
                    break;
                default:
                    $orderBy = 'desc';
                    $orderByField = 'updated_at';
                    break;
            }

              $filterBy = $request['filterby'];
              $filterList = explode (",", $filterBy);
              $filterArray = array();
              foreach ($filterList as $filter){
                  switch ($filter){
                      case ('vegOnly'):
                        $filterArray['product_type'] = '0';
                        break;
                    case ('available'):
                        $filterArray['product_status'] = '1';
                        break;
                  }
              }

              if(!is_null($request['product_category'])){
                $filterArray['productCategory'] = $request['product_category'];
              }
              if((is_null($request['offset']) || $request['offset'] == "0") && is_null($request['limit'])){
                $productList = DB::table('products')
                                    ->orderBy($orderByField, $orderBy)
                                    ->where($filterArray)
                                    ->get();
              }
              else{
                $productList = DB::table('products')
                                    ->where($filterArray)
                                    ->orderBy($orderByField, $orderBy)
                                    ->offset($offset)
                                    ->limit($limit)
                                    ->get();
              }
              return $this->sendResponse($productList, 'Success');
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }
    /*
        API Name        : createProduct
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, name, email, password, phone(O), profile_photo(O)}
        Output Parameter: {id, name, email, phone, profile_photo}

    */
    public function createProduct(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();
            $validator = Validator::make($input, [
                'product_name'      => ['required', 'string', 'max:255','unique:products'],
                'product_category'  => ['required', 'string', 'exists:categories,id'],
                'product_status'    => ['required', 'string', Rule::in(['0', '1'])],
                'product_price'     => ['required', 'numeric'],
                'product_type'      => ['required', 'string', Rule::in(['0', '1', '2'])],
                'product_image'     => ['image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
            ]);


            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            $newProduct = Product::create($input);

            if($request->hasFile('product_image')) {

                $filenamewithextension = $request->file('product_image')->getClientOriginalName();
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                $extension = $request->file('product_image')->getClientOriginalExtension();
                $filenametostore = $filename.'_'.uniqid().'.'.$extension;
                $filenametostore =  preg_replace('/\s+/', '_', $filenametostore);

                $fullFilePath = $request->file('product_image')->store(
                    'images/productImages', 'publicDriver'
                );

                $fileNameDB = env('BASE_URL').'/public'.'/'.$fullFilePath;
                $productInfo = Product::find($newProduct->id);
                $productInfo->product_image = $fileNameDB;
                $productInfo->save();
                return $this->sendResponse($productInfo->toArray(), 'Product created successfully.');
            }
            else{
                return $this->sendResponse($newProduct->toArray(), 'Product created successfully.');
            }

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    /*
        API Name        : updateCategory
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, category_name, category_image(O), category_status(O)}
        Output Parameter: {category_id, category_name, category_image, category_status}

    */
    public function updateProductDetails(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();


            $validator = Validator::make($input, [
                'product_id'        => ['required', 'exists:products,id'],
                'product_name'      => ['string', 'max:255'],
                'product_category'  => ['string', 'exists:categories,id'],
                'product_status'    => ['string', Rule::in(['0', '1'])],
                'product_price'     => ['numeric'],
                'product_type'      => [Rule::in(['0', '1', '2'])],
                'product_image'     => ['image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
            ]);


            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            $currentProduct = Product::find($request['product_id']);

            if(isset($request['product_name'])){
                $currentProduct->product_name = $request['product_name'];
            }
            if(isset($request['product_category'])){
                $currentProduct->product_category = $request['product_category'];
            }
            if(isset($request['product_type'])){
                $currentProduct->product_type = $request['product_type'];
            }
            if(isset($request['product_status'])){
                $currentProduct->product_status = $request['product_status'];
            }
            if(isset($request['product_price'])){
                $currentProduct->product_price = floatval($request['product_price']);
            }
            if($request->hasFile('product_image')) {

                $filenamewithextension = $request->file('product_image')->getClientOriginalName();
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                $extension = $request->file('product_image')->getClientOriginalExtension();
                $filenametostore = $filename.'_'.uniqid().'.'.$extension;
                $filenametostore =  preg_replace('/\s+/', '_', $filenametostore);

                $imageFileSlice = explode("/", $currentProduct->product_image);
                $imageFile = end($imageFileSlice);
                if(\File::exists(public_path('images/productImages/'.$imageFile))){
                    \File::delete(public_path('images/productImages/'.$imageFile));
                }
                $fullPathsss = $request->file('product_image')->store(
                    'images/productImages', 'publicDriver'
                );

                $fileNameDB = env('BASE_URL').'/public'.'/'.$fullPathsss;
                $currentProduct->product_image = $fileNameDB;
            }
            $currentProduct->save();
            return $this->sendResponse($currentProduct->toArray(), 'Product updated successfully.');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    /*
        API Name        : deleteUser
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, user_id}
        Output Parameter: {}

    */
    public function deleteProduct(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();

            $validator = Validator::make($input, [
                'product_id'   => ['required', 'exists:products,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $currentProduct = Product::find($request['product_id']);
            $imageFileSlice = explode("/", $currentProduct->product_image);
            $imageFile = end($imageFileSlice);
            if(\File::exists(public_path('images/productImages/'.$imageFile))){
                \File::delete(public_path('images/productImages/'.$imageFile));
            }
            $currentProduct->delete();
            return $this->sendResponse(null, 'Product deleted successfully.');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    /*
        API Name        : getCategoryDetails
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, category_id}
        Output Parameter: {id, category_name, category_image, category_status}

    */
    public function getProductDetails(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();

            $validator = Validator::make($input, [
                'product_id'   => ['required', 'exists:products,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $currentProduct = Product::find($request['product_id']);
            return $this->sendResponse($currentProduct, 'Success');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

}
