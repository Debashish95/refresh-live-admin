<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

use Validator;
use App\Category;
use Illuminate\Support\Facades\DB;
use File;

class ManageCategoryController extends APIBaseController
{
    /*
        API Name        : getCategoryList
        Method          : GET/POST
        Input Parameter : {authToken}
        Output Parameter: {category_id, category_name, category_image, category_status}

    */
    public function getCategoryList(Request $request)
    {
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){

              $categoryList = Category::all();
              return $this->sendResponse($categoryList, 'Success');
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    /*
        API Name        : createCategory
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, category_name, category_image(O), category_status(O)}
        Output Parameter: {category_id, category_name, category_image, category_status}

    */
    public function createCategory(Request $request)
    {

        $codes = $this->checkAuthToken($request);
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();

            $validator = Validator::make($input, [
                'category_name'     => ['required', 'string', 'max:255','unique:categories'],
                'category_image'    => ['image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
                'category_status'   => [Rule::in(['0', '1'])],
            ]);


            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            $newCategory = Category::create($input);

            if($request->hasFile('category_image')) {

                $filenamewithextension = $request->file('category_image')->getClientOriginalName();
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                $extension = $request->file('category_image')->getClientOriginalExtension();
                $filenametostore = $filename.'_'.uniqid().'.'.$extension;
                $filenametostore =  preg_replace('/\s+/', '_', $filenametostore);

                $fullPathsss = $request->file('category_image')->store(
                    'images/categoryImages', 'publicDriver'
                );

                $fileNameDB = env('BASE_URL').'/public'.'/'.$fullPathsss;
                $categoryInfo = Category::find($newCategory->id);
                $categoryInfo->category_image = $fileNameDB;
                $categoryInfo->save();
                return $this->sendResponse($categoryInfo->toArray(), 'Category created successfully.');
            }
            else{
                return $this->sendResponse($newCategory->toArray(), 'Category created successfully.');
            }

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    /*
        API Name        : updateCategory
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, category_name, category_image(O), category_status(O)}
        Output Parameter: {category_id, category_name, category_image, category_status}

    */
    public function updateCategory(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();


            $validator = Validator::make($input, [
                'category_name' => ['string', 'max:255'],
                'category_image'=> ['image', 'mimes:jpeg,png,jpg,gif', 'max:5120'],
                'category_id'   => ['required', 'exists:categories,id'],
                'category_status'   => [Rule::in(['0', '1'])],
            ]);


            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            $newCategory = Category::find($request['category_id']);

            if(isset($request['category_name'])){
                $newCategory->category_name = $request['category_name'];
            }
            if(isset($request['category_status'])){
                $newCategory->category_status = $request['category_status'];
            }
            if($request->hasFile('category_image')) {

                $filenamewithextension = $request->file('category_image')->getClientOriginalName();
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                $extension = $request->file('category_image')->getClientOriginalExtension();
                $filenametostore = $filename.'_'.uniqid().'.'.$extension;
                $filenametostore =  preg_replace('/\s+/', '_', $filenametostore);

                $imageFileSlice = explode("/", $newCategory->category_image);
                $imageFile = end($imageFileSlice);
                if(\File::exists(public_path('images/categoryImages/'.$imageFile))){
                    \File::delete(public_path('images/categoryImages/'.$imageFile));
                }
                $fullPathsss = $request->file('category_image')->store(
                    'images/categoryImages', 'publicDriver'
                );

                $fileNameDB = env('BASE_URL').'/public'.'/'.$fullPathsss;
                $newCategory->category_image = $fileNameDB;
            }

            $newCategory->save();
            return $this->sendResponse($newCategory->toArray(), 'Category updated successfully.');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    /*
        API Name        : deleteCategory
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, category_id}
        Output Parameter: {}

    */
    public function deleteCategory(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();

            $validator = Validator::make($input, [
                'category_id'   => ['required', 'exists:categories,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $categoryList = Category::find($request['category_id']);
            $imageFileSlice = explode("/", $categoryList->category_image);
            $imageFile = end($imageFileSlice);
            if(\File::exists(public_path('images/categoryImages/'.$imageFile))){
                \File::delete(public_path('images/categoryImages/'.$imageFile));
            }
            $categoryList->delete();
            return $this->sendResponse(null, 'Category deleted successfully.');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    /*
        API Name        : getCategoryDetails
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, category_id}
        Output Parameter: {id, category_name, category_image, category_status}

    */
    public function getCategoryDetails(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();

            $validator = Validator::make($input, [
                'category_id'   => ['required', 'exists:categories,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $categoryList = Category::find($request['category_id']);
            return $this->sendResponse($categoryList, 'Success');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

}
