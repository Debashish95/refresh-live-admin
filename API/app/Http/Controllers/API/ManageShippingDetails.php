<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ShippingCharge;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ManageShippingDetails extends APIBaseController
{

    public function getServiceAreaList(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $input = $request->all();
            $validator = Validator::make($input, [
                'service_id' => ['exists:shipping_charges,id']
            ]);


            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            if(isset($input['service_id'])){
                $serviceAreaList = ShippingCharge::find($input['service_id']);
                return $this->sendResponse($serviceAreaList, "Success");
            }
            else{
                $serviceAreaList = ShippingCharge::all();
                return $this->sendResponse($serviceAreaList, "Success");
            }


        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    public function createServiceArea(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $input = $request->all();

            $validator = Validator::make($input, [
                'locality'              => ['required', 'string'],
                'city'                  => ['required', 'string'],
                'state'                 => ['required', 'string'],
                'country'               => ['required', 'string'],
                'pin_code'              => ['required', 'string'],
                'shipping_charge'       => ['required', 'numeric'],
                'shipping_method'       => ['required', 'string'],
                'average_time'          => ['required', 'numeric'],
                'minimum_order_amount'  => ['required', 'numeric'],
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $newServiceArea = ShippingCharge::create($input);
            return $this->sendResponse($newServiceArea, "Success");

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    public function deleteServiceArea(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $input = $request->all();

            $validator = Validator::make($input, [
                'service_area_id'     => ['required', 'exists:shipping_charges,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            ShippingCharge::find($input['service_area_id'])->delete();
            return $this->sendResponse(null, "Service area deleted successfully.");

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    public function updateServiceArea(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $input = $request->all();
            $validator = Validator::make($input, [
                'service_area_id'     => ['required', 'exists:shipping_charges,id'],
                'locality'              => ['string'],
                'city'                  => ['string'],
                'state'                 => ['string'],
                'country'               => ['string'],
                'pin_code'              => ['string'],
                'shipping_charge'       => ['numeric'],
                'shipping_method'       => ['string'],
                'average_time'          => ['numeric'],
                'minimum_order_amount'  => ['numeric'],
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $serviceArea = ShippingCharge::find($input['service_area_id']);
            if(isset($input['locality'])){
                $serviceArea->locality = $input['locality'];
            }
            if(isset($input['city'])){
                $serviceArea->city = $input['city'];
            }
            if(isset($input['state'])){
                $serviceArea->state = $input['state'];
            }
            if(isset($input['country'])){
                $serviceArea->country = $input['country'];
            }
            if(isset($input['pin_code'])){
                $serviceArea->pin_code = $input['pin_code'];
            }
            if(isset($input['shipping_charge'])){
                $serviceArea->shipping_charge = (doubleval($input['shipping_charge']));
            }
            if(isset($input['shipping_method'])){
                $serviceArea->shipping_method = $input['shipping_method'];
            }
            if(isset($input['average_time'])){
                $serviceArea->average_time = $input['average_time'];
            }
            if(isset($input['minimum_order_amount'])){
                $serviceArea->minimum_order_amount = (doubleval($input['minimum_order_amount']));
            }
            $serviceArea->save();
            return $this->sendResponse($serviceArea, "Success");

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }
}
