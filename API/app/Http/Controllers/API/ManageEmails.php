<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\SendMarketingEmailsJob;
use App\Mail\MarketingMail;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

use Validator;
use Illuminate\Support\Facades\DB;


class ManageEmails extends APIBaseController
{
    public function sendEmail(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $to_emails = $request['emailAddress'];
            $emailRecipients = preg_split ("/\,/", $to_emails);
            if($to_emails == "" || (count($emailRecipients) == 0)){
                return $this->sendErrorResponse(400, [], 'No recipient found.');
            }
            $data = ['subject'=>$request['emailSubject'], 'body'=>$request['emailBody']];

            foreach($emailRecipients as $email){
                $emailJob = (new SendMarketingEmailsJob($email, $data))
                                                    ->delay(Carbon::now()->addSeconds(5));
                dispatch($emailJob);
            }

            return $this->sendResponse(null, 'Email sent successfully.');

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    public function sendDummyEmail(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $to_emails = $request['emailAddress'];
            $emailRecipients = preg_split ("/\,/", $to_emails);
            if($to_emails == "" || (count($emailRecipients) == 0)){
                return $this->sendErrorResponse(400, [], 'No recipient found.');
            }
            $data = ['subject'=>$request['emailSubject'], 'body'=>$request['emailBody']];

            foreach($emailRecipients as $email){
                Mail::to($email)->send(new MarketingMail($data));
            }

            return $this->sendResponse(null, 'Email sent successfully.');

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }
}
