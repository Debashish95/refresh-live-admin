<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Validator;
use App\User;
use App\Admin;
use App\AdminUser;
use Illuminate\Support\Facades\DB;
use File;

class UsersAPIController extends APIBaseController
{
    /*
        API Name        : getUserList
        Method          : GET/POST
        Input Parameter : {authToken}
        Output Parameter: {category_id, category_name, category_image, category_status}
        
    */
    public function getUserList(Request $request)
    {
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
      
              $userList = User::all();
              return $this->sendResponse($userList, 'Success');
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
        
    }
    /*
        API Name        : createUser
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, name, email, password, phone(O), profile_photo(O)}
        Output Parameter: {id, name, email, phone, profile_photo}
        
    */
    public function createUser(Request $request)
    {
        $userList = null;
        $codes = $this->checkAuthToken($request);

        $userList = new User();
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();


            $validator = Validator::make($input, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'email' ,'string', 'max:255', 'unique:users'],
                'password'=> ['required', 'string', 'min:6'],
                'phone_number'=> ['required', 'numeric', 'min:10'],
                'profile_image'=> ['image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
            ]);


            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);  
            }
            $input['password'] = md5($input['password']);
            $newUser = User::create($input);
            $input['password'] = Hash::make($input['password']);
            if($request->hasFile('profile_image')) {

                $filenamewithextension = $request->file('profile_image')->getClientOriginalName();
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                $extension = $request->file('profile_image')->getClientOriginalExtension();
                $filenametostore = $filename.'_'.uniqid().'.'.$extension;
                $filenametostore =  preg_replace('/\s+/', '_', $filenametostore);
                
                $fullFilePath = $request->file('profile_image')->store(
                    'images/profileImages', 'publicDriver'
                );
                
                $fileNameDB = env('BASE_URL').'/public'.'/'.$fullFilePath;
                $userInfo = User::find($newUser->id);
                $userInfo->profile_image = $fileNameDB;
                $userInfo->save();
                return $this->sendResponse($userInfo->toArray(), 'User created successfully.');
            }
            else{
                return $this->sendResponse($newUser->toArray(), 'User created successfully.');
            }

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
        
    }

    /*
        API Name        : updateCategory
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, category_name, category_image(O), category_status(O)}
        Output Parameter: {category_id, category_name, category_image, category_status}
        
    */
    public function updateProfileDetails(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();


            $validator = Validator::make($input, [
                'user_id'       => ['required', 'exists:users,id'],
                'name'          => ['string', 'max:255'],
                'email'         => ['required', 'email' ,'string', 'max:255'],
                'password'      => ['string', 'min:6'],
                'phone_number'  => ['numeric', 'min:10'],
                'profile_image' => ['image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
            ]);


            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);  
            }
            
            $currentUser = User::find($request['user_id']);
            
            if($currentUser->email != $request['email']){
                return $this->sendErrorResponse(400, [], 'User id does not match with email.');
            }

            if(isset($request['name'])){
                $currentUser->name = $request['name'];
            }
            if(isset($request['phone_number'])){
                $currentUser->phone_number = $request['phone_number'];
            }
            if($request->hasFile('profile_image')) {

                $filenamewithextension = $request->file('profile_image')->getClientOriginalName();
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                $extension = $request->file('profile_image')->getClientOriginalExtension();
                $filenametostore = $filename.'_'.uniqid().'.'.$extension;
                $filenametostore =  preg_replace('/\s+/', '_', $filenametostore);
                
                $imageFileSlice = explode("/", $currentUser->profile_image);
                $imageFile = end($imageFileSlice);
                if(\File::exists(public_path('images/profileImages/'.$imageFile))){
                    \File::delete(public_path('images/profileImages/'.$imageFile));
                }
                $fullPathsss = $request->file('profile_image')->store(
                    'images/profileImages', 'publicDriver'
                );
                
                $fileNameDB = env('BASE_URL').'/public'.'/'.$fullPathsss;
                $currentUser->profile_image = $fileNameDB;
            }

            $currentUser->save();
            return $this->sendResponse($currentUser->toArray(), 'User profile updated successfully.');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
        
    }

    /*
        API Name        : deleteUser
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, user_id}
        Output Parameter: {}
        
    */
    public function deleteUser(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();

            $validator = Validator::make($input, [
                'user_id'   => ['required', 'exists:users,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);  
            }
            $currentUser = User::find($request['user_id']);
            $imageFileSlice = explode("/", $currentUser->profile_image);
            $imageFile = end($imageFileSlice);
            if(\File::exists(public_path('images/profileImages/'.$imageFile))){
                \File::delete(public_path('images/profileImages/'.$imageFile));
            }
            $currentUser->delete();
            return $this->sendResponse(null, 'User deleted successfully.');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
        
    }

    /*
        API Name        : getCategoryDetails
        Method          : POST[multipart/form-data]
        Input Parameter : {authToken, category_id}
        Output Parameter: {id, category_name, category_image, category_status}
        
    */
    public function getProfileDetails(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();

            $validator = Validator::make($input, [
                'user_id'   => ['required', 'exists:users,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);  
            }
            $currentUser = User::find($request['user_id']);
            return $this->sendResponse($currentUser, 'Success');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
        
    }
    public function createAdmin(Request $request)
    {
        $codes = $this->checkAuthToken($request);
        $allUsers = new AdminUser();
        if(($codes['code'] == 200)){
            $input = $request->all();
            $validator = Validator::make($input, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:admins'],
                'phone' => ['required','numeric', 'min:10'],
                'password' => ['required', 'string', 'min:6'],
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            $input['password'] = Hash::make($input['password']);
            $newUser = AdminUser::create($input);
            return $this->sendResponse($newUser->toArray(), 'Admin created successfully.');
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
        
    }

    public function loginAdmin(Request $request)
    {
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){
            $input = $request->all();
            $validator = Validator::make($input, [
                'email' => ['required', 'string', 'email', 'max:255'],
                'password' => ['required', 'string', 'min:6'],
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            $codes = $this->checkAuthToken($request);
            $userList = AdminUser::all();
            $email = $input['email'];
            $password = Hash::make($input['password']);

            foreach($userList as $admin){
                if(($email == $admin->email) && ($password == $admin->password)){
                    return $this->sendResponse($admin, 'Success');
                }
            }
            
            return $this->sendErrorResponse(400, [], 'Email or password is invalid.');
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
        
    }
}