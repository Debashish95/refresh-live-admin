<?php

namespace App\Http\Controllers\API;

use App\Cart;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\DB;


class ManageCart extends APIBaseController
{
    public function addToCart(Request $request){

        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){

            $input = $request->all();
            $currentProductQuantity = Product::find($input['product_id'])->product_quantity ?? 0;

            $validator = Validator::make($input, [
                'user_id'     => ['required', 'exists:users,id'],
                'product_id'  => ['required', 'exists:products,id'],
                'product_quantity' => ['numeric', 'min:1', 'max:'.$currentProductQuantity],
            ]);
            //['numeric', ]

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            $cartDetails = Cart::where('user_id',$request['user_id'])->first();
            $cartOperationMessage = '';
            if(is_null($cartDetails)){
                $input['cart_id'] = uniqid();
                $newCart = Cart::create($input);
                $cartDetails = $newCart;
                $cartOperationMessage = 'Added to cart successfully.';
            }else{
                $input['cart_id'] = $cartDetails->cart_id;
                $cartProductDetails = Cart::where([['product_id', '=',$request['product_id']],
                                                ['user_id', '=',$request['user_id']]])->first();
                if(is_null($cartProductDetails)){
                    $newCart = Cart::create($input);
                    $cartDetails = $newCart;
                    $cartOperationMessage = 'Added to cart successfully.';
                }else{
                    if(isset($input['product_quantity'])){
                        $quantityToAdd = (int)$input['product_quantity'];
                        $cartProductDetails->product_quantity = (string)$quantityToAdd;
                    }else{
                        $productQuatity = (int)$cartProductDetails->product_quantity;
                        $productQuatity += 1;
                        $cartProductDetails->product_quantity = (string)$productQuatity;
                    }
                    $cartProductDetails->save();
                    $cartDetails = $cartProductDetails;
                    $cartOperationMessage = 'Cart updated successfully.';
                }

            }

            $cartCompleteDetails = DB::table('carts')->where('user_id', $input['user_id'])->get();
            $totalItems = 0;
            $totalPrice = 0.0;

            foreach($cartCompleteDetails as $item){
                $totalItems += (int)$item->product_quantity;
                $currentProductPrice = Product::find($item->product_id)->product_price;
                $totalPrice += ($currentProductPrice * ((int)$item->product_quantity));
            }

            $cartOutPut['cart_items'] = $cartDetails;
            $cartOutPut['total_items'] = $totalItems;
            $cartOutPut['unique_items'] = count($cartCompleteDetails);
            $cartOutPut['total_price'] = $totalPrice;
            return $this->sendResponse($cartOutPut, $cartOperationMessage);
        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }


    }

    public function removeFromCart(Request $request){

        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){

            $input = $request->all();
            $validator = Validator::make($input, [
                'user_id'     => ['required', 'exists:users,id'],
                'product_id'  => ['required', 'exists:carts'],
                'cart_id'     => ['required', 'exists:carts'],
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            $cartDetails = Cart::where('product_id',$request['product_id'])->first();
            $cartDetails->delete();

            $cartCompleteDetails = DB::table('carts')->where('user_id', $input['user_id'])->get();
            $totalItems = 0;
            $totalPrice = 0.0;

            foreach($cartCompleteDetails as $item){
                $totalItems += (int)$item->product_quantity;
                $currentProductPrice = Product::find($item->product_id)->product_price;
                $totalPrice += ($currentProductPrice * ((int)$item->product_quantity));
            }

            $cartOutPut['total_items'] = $totalItems;
            $cartOutPut['unique_items'] = count($cartCompleteDetails);
            $cartOutPut['total_price'] = $totalPrice;
            return $this->sendResponse($cartOutPut, 'Product deleted from cart successfully.');

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    public function getCartList(Request $request){

        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){

            $input = $request->all();
            $validator = Validator::make($input, [
                'user_id'     => ['required', 'exists:users,id'],
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            $cartDetails = DB::table('carts')->where('user_id', $input['user_id'])->get();
            $totalItems = 0;
            $totalPrice = 0.0;

            foreach($cartDetails as $item){
                $totalItems += (int)$item->product_quantity;
                $currentProductPrice = Product::find($item->product_id)->product_price;
                $totalPrice += ($currentProductPrice * ((int)$item->product_quantity));
            }

            $cartOutPut['cart_items'] = $cartDetails;
            $cartOutPut['total_items'] = $totalItems;
            $cartOutPut['unique_items'] = count($cartDetails);
            $cartOutPut['total_price'] = $totalPrice;
            return $this->sendResponse($cartOutPut, 'Success');

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

}
