<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

use Notification;
use App\User;
use App\Events\sendEmails;
use Mailgun\Mailgun;

use App\Notifications\EmailNotification;
use RobinCSamuel\LaravelMsg91\Facades\LaravelMsg91;


class MailAPI extends APIBaseController
{
    //
    public function index(Request $request)
    {
        
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){

            $emailSubject = $request['subject'];
            $emailBody    = $request['body'];

            $emails = $request['userList'];
            $emailsAddress = explode (",", $emails);
            $newEmailString = "";
            foreach($emailsAddress as $emailID){
                $newEmailString = $newEmailString . "'" . $emailID. "',";
            }
            $newEmailString = substr($newEmailString, 0, -1);
            $message = $emailBody;
            $details = [
                'greeting' => 'Hi There',
                'body' => $emailBody,
                'thanks' => 'Thank you for being a member of RefreshLive family.',
                'actionText' => 'Visit Our Site',
                'actionURL' => url('https://silgul.com/refreshlive'),
                'subject' => $emailSubject,
                'salute' => 'Yours Faithfully'
            ];
            $users = DB::table('users')
                            ->whereRaw("email IN (". $newEmailString. ")" , [200])
                            ->get();
            $finalUserList = [];                
            foreach($users as $user){
                array_push($finalUserList, $user);
            }
            // dd($users, $finalUserList, User::find('1'));
            $data = [
                'details' => $details,
                'newUsers' => $emailsAddress
            ];
            

           
            // foreach($emailsAddress as $emailMain){
            //    Mail::send('welcome', [], function($message) use ($emailMain, $emailSubject)
            //     {    
            //         $message->to($emailMain)->subject($emailSubject);    
            //     }); 
            // }
            

            /*# Instantiate the client.
            $mgClient = Mailgun::create(env('MAILGUN_SECRET')); // For US servers
            $domain = 'marketingemail.silgul.com';//env('MAILGUN_DOMAIN');
            
            # Make the call to the client.
            // dd($emailsAddress);
            foreach($emailsAddress as $email){
                
                $result = $mgClient->messages()->send($domain, array(
                    'from'	=> 'kanha.debashish@gmail.com',
                    'to'	=> $email,
                    'subject' => $emailSubject,
                    'text'	=> $emailBody
                ));
            }*/
            // $users->notify(new EmailNotification($data));
            // Notification::send('debashishdash03@gmail.com', new EmailNotification($data));
            event(new sendEmails($data));
            // $data1 = ['data_item' => $details];
            // foreach($emailsAddress as $emailMain){
            //     Mail::send('admin.emailTemplate', ['data_item' => $details], function($message) use ($emailMain, $emailSubject)
            //     {
            //         $message->to($emailMain)->subject($emailSubject);
            //     });
                
            // }
            
            // return $this->sendResponse(null, 'Success');
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
        
    }

    function sendSMS(Request $request){

        dd($request->all());
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $result = LaravelMsg91::message($request['mobileNumber'], $request['message']);
            dd($result->type, $result->message);
            return json_encode($result);
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
        
        dd($result);
    }
    
}
