<?php

namespace App\Http\Controllers\API;

use App\Address;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ManageAddressController extends APIBaseController
{

    public function getAddressList(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $input = $request->all();

            $validator = Validator::make($input, [
                'user_id'     => ['required', 'exists:users,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $addressList = DB::table('addresses')->where('user_id', $input['user_id'])->get();
            return $this->sendResponse($addressList, "Success");

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    public function createAddress(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $input = $request->all();

            //user_id, address_name, address_line_1,
            //address_line_2, land_mark, city, pin, mobile_number, alternate_mobile_number
            $validator = Validator::make($input, [
                'user_id'                   => ['required', 'exists:users,id'],
                'address_name'              => ['required', 'string'],
                'address_line_1'            => ['required', 'string'],
                'address_line_2'            => ['string'],
                'land_mark'                 => ['string'],
                'locality'                  => ['required', 'string'],
                'city'                      => ['string'],
                'state'                     => ['string'],
                'country'                   => ['string'],
                'pin_code'                  => ['required','string'],
                'mobile_number'             => ['required','string'],
                'alternate_mobile_number'   => ['string'],

            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $addressList = DB::table('addresses')->where('user_id', $input['user_id'])->get();
            return $this->sendResponse($addressList, "Success");

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    public function deleteAddress(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $input = $request->all();

            $validator = Validator::make($input, [
                'user_id'     => ['required', 'exists:users,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $addressList = DB::table('addresses')->where('user_id', $input['user_id'])->get();
            return $this->sendResponse($addressList, "Success");

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    public function updateAddress(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $input = $request->all();

            $validator = Validator::make($input, [
                'user_id'     => ['required', 'exists:users,id']
            ]);

            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $addressList = DB::table('addresses')->where('user_id', $input['user_id'])->get();
            return $this->sendResponse($addressList, "Success");

        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }
}
