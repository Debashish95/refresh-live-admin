<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Validator;
use App\Info;
use Illuminate\Support\Facades\DB;


class ManageInfoAPI extends APIBaseController
{
    public function getAllInfo(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $infoList = DB::table('infos')
                                ->get();
            return $this->sendResponse($infoList, 'Success');
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    public function createInfo(Request $request)
    {
        $infoList = null;
        $codes = $this->checkAuthToken($request);

        $infoList = new Info();
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $input = $request->all();


            $validator = Validator::make($input, [
                'key' => ['required', 'string', 'max:255','unique:infos'],
            ]);


            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }

            $newInfo = Info::create($input);
            return $this->sendResponse($newInfo->toArray(), $input['key'].' created successfully.');

        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    public function getInfo(Request $request)
    {

        $infoList = null;
        $codes = $this->checkAuthToken($request);

        $infoList = new Info();
        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $id = $request['id'];
            $infoList = Info::find($id);

            if (is_null($id)) {
                return $this->sendErrorResponse(409, [], 'Info not found.');
            }
            if (is_null($infoList)) {
                return $this->sendErrorResponse(409, [], 'Info id not found.');
            }
            return $this->sendResponse($infoList, 'Success');
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    public function updateInfo(Request $request)
    {

        $infoList = null;
        $codes = $this->checkAuthToken($request);

        $codes = $this->checkAuthToken($request);

        if(($codes['code'] == 200)){

            $id = $request['id'];
            $infoKey = $request['key'];

            $infoList = Info::find($id);

            if (is_null($id)) {
                return $this->sendErrorResponse(409, [], 'Info id not found.');
            }
            if (is_null($infoList)) {
                return $this->sendErrorResponse(409, [], 'Info not found.');
            }

            if ($infoList['key'] != $infoKey) {
                return $this->sendErrorResponse(409, [], 'Info key mismatch.');
            }

            if (isset($request['value'])) {
                $infoList->value = $request['value'];
            }
            $infoList->save();
            return $this->sendResponse($infoList, 'Success');
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }

    }

    public function getDashboardData(Request $request)
    {

    }

}
