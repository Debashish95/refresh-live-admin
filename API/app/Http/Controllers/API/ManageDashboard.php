<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ManageDashboard extends APIBaseController
{
    public function getDashboardData(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $output['total_orders'] = 0;
            $output['total_users'] = DB::table('users')->get()->count();
            $output['total_products'] = DB::table('products')->get()->count();
            $output['total_categories'] = DB::table('categories')->get()->count();
            $output['total_shipping_area'] = DB::table('shipping_charges')->get()->count();
            $output['unique_visits'] = DB::table('visitors')->select('ip')->groupBy('ip')->get()->count();
            $output['total_income'] = 0;
            $output['total_support_tickets'] = 0;

            return $this->sendResponse($output, 'Success');
        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    //
    public function getIPAddress(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $ipTask = curl_init();
            curl_setopt($ipTask, CURLOPT_URL, 'https://api.ipify.org');
            curl_setopt($ipTask, CURLOPT_RETURNTRANSFER, true);
            $ipData = curl_exec($ipTask);
            curl_close($ipTask);

            $ipTaskDetails = curl_init();
            curl_setopt($ipTaskDetails, CURLOPT_URL, 'https://ipinfo.io/'. $ipData .'/json');
            curl_setopt($ipTaskDetails, CURLOPT_RETURNTRANSFER, true);
            $ipDataDetails = json_decode(curl_exec($ipTaskDetails));
            curl_close($ipTaskDetails);

            unset($ipDataDetails->readme);
            unset($ipDataDetails->hostname);
            return $this->sendResponse($ipDataDetails, 'Success');
        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }
}
