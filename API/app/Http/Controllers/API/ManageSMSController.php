<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Support\Facades\DB;
use RobinCSamuel\LaravelMsg91\Facades\LaravelMsg91;

class ManageSMSController extends APIBaseController{
    /*
        API Name        : sendSMS
        Method          : GET/POST
        Input Parameter : {authToken, mobileNumber}
        Output Parameter: {}
        
    */
    public function sendSMS(Request $request)
    {
        $codes = $this->checkAuthToken($request);
        if(!isset($request['mobileNumber'])){
            return $this->sendErrorResponse(400, [], 'Please provide mobile numbers.');
        }
        $phoneNumbers = $request['mobileNumber'];
        $mobileNumbers = explode(",", $phoneNumbers);
        $message = urldecode($request['message']);

        if(($codes['code'] == 200)){
            $result = LaravelMsg91::message($mobileNumbers, $message);
            if($result->type == "success"){
                return $this->sendResponse($result, 'Success');
            }
            else{
                return $this->sendErrorResponse(400, [], $result->message);
            }
        }
        else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }
}