<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;


class ManageVisitors extends APIBaseController
{
    public function getVisitorsData(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){
            $visitors = Visitor::all();
            return $this->sendResponse($visitors, 'Success');
        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }

    //
    public function createVisitor(Request $request){
        $codes = $this->checkAuthToken($request);
        if(($codes['code'] == 200)){

            $input = $request->all();
            /*"ip": "139.59.25.93",
            "city": "Dod Ballāpur",
            "region": "Karnataka",
            "country": "IN",
            "loc": "13.2257,77.5750",
            "org": "AS14061 DigitalOcean, LLC",
            "postal": "560100",
            "timezone": "Asia\/Kolkata"*/
            $validator = Validator::make($input, [
                'ip'        => ['required', 'string'],
                'city'      => ['required', 'string'],
                'region'    => ['required', 'string'],
                'country'   => ['required', 'string'],
                'latitude'  => ['required', 'string'],
                'longitude' => ['required', 'string'],
                'org'       => ['required', 'string'],
                'org'       => ['required', 'string'],
                'postal'    => ['required', 'string'],
                'timezone'  => ['required', 'string'],
                'name'      => ['string', 'exists:users,name'],
                'email'     => ['email', 'string', 'exists:users,email'],
            ]);


            if($validator->fails()){
                $errorMessage = $validator->errors()->all();
                return $this->sendErrorResponse(400, [], $errorMessage[0]);
            }
            $newVisitor = Visitor::create($input);
            return $this->sendResponse($newVisitor, 'Success');
        }else{
            return $this->sendErrorResponse($codes['code'], [], $codes['message']);
        }
    }
}
