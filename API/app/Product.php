<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'product_name', 'product_image', 'product_category', 
        'product_price', 'product_type', 'product_status'
    ];
}
