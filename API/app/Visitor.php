<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{

    protected $fillable = [
        'ip', 'city', 'region', 'latitude', 'longitude',
        'org', 'postal', 'timezone', 'name', 'email'
    ];
}
