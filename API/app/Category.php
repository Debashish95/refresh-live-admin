<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable = [
        'category_name', 'category_image', 'category_status'
    ];
    protected $attributes = [
        'category_status' => '0',
     ];
 
}
