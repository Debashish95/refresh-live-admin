<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MarketingMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $emailBody = $this->data;
        // $emailBody = $emailBody['body'];
        return $this->from(env('MAIL_FROM_ADDRESS', "admin@gmail.com"), env('MAIL_FROM_NAME', "Authores TechPark"))
                    ->subject($this->data['subject'])
                    ->view('email.marketing', ['data' => $this->data]);
    }
}
