<?php

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use App\Mail\MarketingMail;

class SendMarketingEmailsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $data, $emailRecipient;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emailRecipient, $data)
    {
        //
        $this->emailRecipient = $emailRecipient;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Mail::to($this->emailRecipient)->send(new MarketingMail($this->data));
    }
}

