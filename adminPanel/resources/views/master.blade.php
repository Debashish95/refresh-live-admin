<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RefreshLive | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{url('public/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('public/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{url('public/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('public/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{url('public/dist/css/skins/_all-skins.min.css')}}">

  <!-- jvectormap -->
  <link rel="stylesheet" href="{{url('public/bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{url('public/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{url('public/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{url('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

  <!-- DataTables -->
  <link rel="stylesheet" href="{{url('public/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="{{url('public/bower_components/select2/dist/css/select2.min.css')}}">
  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>R</b>LIVE</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Refresh</b>Live</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{url('public/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
              <span class="hidden-xs">RefreshLive Admin</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{url('public/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

                <p>
                RefreshLive Admin
                  <small>Member since Nov. 2012</small>
                </p>
              </li>

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="/logout" class="btn btn-danger btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('public/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>RefreshLive Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><b>Analytics</b></li>
        <li class="{{ Request::is('/') ? 'active' : '' }}">
          <a href="/">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="{{ Request::is('manageOrders') ? 'active' : '' }}">
          <a href="manageOrders">
            <i class="ion ion-bag"></i> <span>Manage Orders</span>
          </a>
        </li>

        <li class="header"><b>Product Section</b></li>
        <li class="{{ Request::is('manageCategory') ? 'active' : '' }}">
          <a href="/manageCategory">
            <i class="fa fa-database"></i> <span>Manage Category</span>
          </a>
        </li>

        <li class="{{ Request::is('manageProducts') ? 'active' : '' }}">
          <a href="manageProducts">
            <i class="fa fa-cutlery"></i> <span>Manage Products</span>
          </a>
        </li>
        <li class="{{ Request::is('manageGallery') ? 'active' : '' }}">
          <a href="manageGallery">
            <i class="fa fa-file-image-o"></i> <span>Manage Gallery</span>
          </a>
        </li>

        <li class="header"><b>User Section</b></li>
        <li class="{{ Request::is('manageUser') ? 'active' : '' }}">
          <a href="manageUser">
            <i class="fa fa-users"></i> <span>Manage Users</span>
          </a>
        </li>
        {{-- <li class="{{ Request::is('manageWallet') ? 'active' : '' }}">
          <a href="/manageWallet">
            <i class="fa fa-users"></i> <span>Manage Wallets</span>
          </a>
        </li> --}}
        {{-- <li class="{{ Request::is('manageReward') ? 'active' : '' }}">
          <a href="/manageReward">
            <i class="fa fa-users"></i> <span>Manage Rewards</span>
          </a>
        </li> --}}
        <li class="{{ Request::is('manageServiceArea') ? 'active' : '' }}">
            <a href="manageServiceArea">
              <i class="fa fa-truck"></i> <span>Manage Service Area</span>
            </a>
          </li>

        <li class="header"><b>SMS & Notifications</b></li>
        <li class="{{ Request::is('sendSMS') ? 'active' : '' }}">
          <a href="sendSMS">
            <i class="fa fa-comment-o"></i> <span>Send SMS</span>
          </a>
        </li>
        <li class="{{ Request::is('sendEmail') ? 'active' : '' }}">
          <a href="sendEmail">
            <i class="fa fa-envelope-o"></i> <span>Send Email</span>
          </a>
        </li>

        <li class="header"><b>Miscellaneous</b></li>
        <li class="{{ Request::is('about-us') ? 'active' : '' }}">
          <a href="about-us">
            <i class="fa fa-info-circle"></i> <span>About Us</span>
          </a>
        </li>
        <li class="{{ Request::is('terms-condition') ? 'active' : '' }}">
          <a href="terms-condition">
            <i class="fa fa-shield"></i> <span>Terms and Conditions</span>
          </a>
        </li>
        <li class="{{ Request::is('privacy-policy') ? 'active' : '' }}">
          <a href="privacy-policy">
            <i class="fa fa-shield"></i> <span>Privacy Policy</span>
          </a>
        </li>
        <li class="{{ Request::is('setting') ? 'active' : '' }}">
          <a href="setting">
            <i class="fa fa-cog"></i> <span>Settings</span>
          </a>
        </li>
      </ul>
    </section>
    </aside>
    <!-- /.sidebar -->

  <!-- content-wrapper -->
    @if(Request::is('manageUser'))
            @yield('manageUsers')
        @elseif(Request::is('manageCategory'))
            @yield('managecategory')
        @elseif(Request::is('manageProducts'))
            @yield('manageProducts')
        @elseif(Request::is('manageGallery'))
            @yield('manageGallery')
        @elseif(Request::is('manageNotification'))
            @yield('manageNotification')
        @elseif(Request::is('sendSMS'))
            @yield('manageSMS')
        @elseif(Request::is('sendEmail'))
            @yield('manageEmails')
        @elseif(Request::is('manageServiceArea'))
            @yield('manageServiceArea')
        @elseif(Request::is('manageOrders'))
            @yield('manageServiceArea')
        @elseif(Request::is('/'))
            @yield('dashboard')
    @endif


  <!-- /.content-wrapper -->



  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.1
    </div>
    <strong>Copyright &copy; 2019-2020 <a href="https://www.authores.com">Authores TechPark</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{url('public/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{url('public/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{url('public/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Sparkline -->
<script src="{{url('public/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{url('public/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{url('public/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{url('public/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{url('public/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{url('public/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{url('public/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{url('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{url('public/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{url('public/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('public/dist/js/adminlte.min.js')}}"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{url('public/dist/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{url('public/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('public/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- Select2 -->
<script src="{{url('public/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

<!-- CK Editor -->
<script src="{{url('public/bower_components/ckeditor/ckeditor.js')}}"></script>
<script>
  $(function () {
    CKEDITOR.replace('emailTextArea')
    $('.select2').select2()

    $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
    })
  })
</script>
</body>
</html>
