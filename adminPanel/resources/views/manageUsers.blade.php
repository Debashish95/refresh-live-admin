@extends('master')

@section('manageUsers')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Users
        <small>Manage the user list</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Manage Users</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">User List</h3>
        </div>
        <div class="box-body">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">&nbsp;</h3>
              <button class="btn btn-block btn-primary btn-flat" style="width: 150px;"
              data-toggle="modal" data-target="#modal-default"
              onclick="clearAllField()">Add User</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>User Name</th>
                  <th>User Image</th>
                  <th>User Info</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          &nbsp;
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addUserLabel">Add User</h4>
              </div>
              <div class="modal-body">
              <form class="form-horizontal">
                <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">User Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Enter User Name" id="userName">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">User Email</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Enter Email Address" id="userEmail">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">User Password</label required password>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Enter Password" id="userPassword">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Mobile Number</label required password>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Enter Mobile Number" id="userPhone">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">User Image</label>
                  <div class="col-sm-10">
                  <img src="" style="width: 150px; height: 150px" id="userImage" name="userImage">
                  <input type="file" id="fileToUpload" accept="image/*" onchange="changeImage()">
                  </div>
                </div>
                </div>
              </form>
              
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" 
                id="closeButton" onclick="clearAllField()">Close</button>
                <button type="button" class="btn btn-primary" id="saveButton" onclick="createCategory()">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>

$(document).ready(function(){
  clearAllField();
  getUserList();
});

function changeImage(){
    var userImage = document.getElementById("fileToUpload").files[0];
    if (typeof userImage !== 'undefined'){
        var fileExtension = userImage.type.split('/').pop().toLowerCase();
        if (fileExtension != "jpeg" && fileExtension != "jpg" && fileExtension != "png") {
            alert('Please select a valid image file');
            document.getElementById("fileToUpload").value = '';
            document.getElementById("userImage").style.display = 'none';
            return;
        }
        else if (userImage.size > 2048000) {
            alert("Max image size is 2MB only");
            document.getElementById("fileToUpload").value = '';
            document.getElementById("userImage").style.display = 'none';
            return;
        }
        else{
            document.getElementById("userImage").style.display = 'block';
            document.getElementById("userImage").src =  window.URL.createObjectURL(userImage);
        } 
    }
}


function clearAllField(){
  $("#categoryStatus option").prop('selected',false);
  document.getElementById("userName").value = "";
  document.getElementById("userName").required = false;
  document.getElementById("userEmail").value = "";
  document.getElementById("userName").required = false;
  document.getElementById("userPassword").value = "";
  document.getElementById("userPassword").required = false;
  document.getElementById("userPhone").value = "";
  document.getElementById("userPhone").required = false;
  document.getElementById("fileToUpload").value = '';
  document.getElementById("userImage").style.display = 'none';
  document.getElementById("addUserLabel").innerHTML  = 'Add New User';
  
  document.getElementById("closeButton").disabled = false;
  document.getElementById("saveButton").disabled = false
  document.getElementById("saveButton").onclick = function(){
    createUser();
  }
}

function deleteAction(categoryId) {
  swal({
    title: "Alert!",
    text: "Are you sure you want to delete the user?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      deleteUser(categoryId);
    }
  });
}


// API Call handles here
async function getUserList(){
    const endPoint = '{{env("APP_BASE_URL", "")}}/getAllUsers';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          var index = 0;
          var userTable = $('#example1').DataTable();
          userTable.clear();
          $.each(resultJSON.data, function(key, value){
            index += 1;
            let userImage = `<img src="${value.profile_image}" style="width: 50px; height: 50px;" alt= "...">`;
            let actionButton = `<div class="btn-group">
                  <button type="button" class="btn btn-default btn-flat">Action</button>
                  <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    
                    <li><a onclick="getProfileDetails(${value.id})" data-toggle="modal" data-target="#modal-default">Edit</a></li>
                    <li><a onclick="deleteAction(${value.id})">Delete</a></li>
                  </ul>
                </div>`;
            let userInfo = `
            <label>Mobile: ${value.phone_number}</label>
            <label>Email: ${value.email}</label>
            `;
            userTable.row.add([
              index, value.name, userImage, userInfo, actionButton
            ]).draw();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }

}


async function createUser(){
    let userName = document.getElementById("userName").value;
    let userEmail = document.getElementById("userEmail").value;
    let userPassword = document.getElementById("userPassword").value;
    let userPhone = document.getElementById("userPhone").value;
    let userImage = document.getElementById("fileToUpload").files[0];

    if((userName.trim() == "") || (userEmail.trim() == "") || (userPassword.trim() == "") 
    || (userPhone.trim() == "")){
      swal({
        title: "Alert!",
        text: 'Please enter all fields.',
        icon: "warning"
      });
      return;
    }

    const endPoint = '{{env("APP_BASE_URL", "")}}/registerUser';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('name', userName);
    formData.append('email', userEmail);
    formData.append('password', userPassword);
    formData.append('phone_number', userPhone);
    if (typeof userImage !== 'undefined'){
      formData.append('profile_image', userImage);
    }
    

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          }).then(function(){
            clearAllField();
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      }).then(function(){
        clearAllField();
      });
    }

}


async function deleteUser(userId){
    const endPoint = '{{env("APP_BASE_URL", "")}}/deleteUser';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('user_id', userId);
    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}


async function getProfileDetails(userId){
    const endPoint = '{{env("APP_BASE_URL", "")}}/getProfileDetails';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('user_id', userId);
    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){ 
          document.getElementById("userName").value = resultJSON.data.name;
          document.getElementById("userEmail").value = resultJSON.data.email;
          document.getElementById("userEmail").disabled = true;
          document.getElementById("userPhone").value = resultJSON.data.phone_number;
          document.getElementById("userImage").src = resultJSON.data.profile_image;
          document.getElementById("userImage").style.display = 'block';
          document.getElementById("addUserLabel").innerHTML  = 'Edit User';
          document.getElementById("saveButton").onclick = function(){
            updateProfileDetails(userId);
          }
          
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}


async function updateProfileDetails(userId){

    let userName = document.getElementById("userName").value;
    let userEmail = document.getElementById("userEmail").value;
    let userPassword = document.getElementById("userPassword").value;
    let userPhone = document.getElementById("userPhone").value;
    let userImage = document.getElementById("fileToUpload").files[0];
    
    const endPoint = '{{env("APP_BASE_URL", "")}}/updateProfileDetails';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('user_id', userId);
    formData.append('email', userEmail);
    if(userName.trim() != ""){
    	formData.append('name', userName);
    }
    if(userPassword.trim() != ""){
		formData.append('password', userPassword);
	}
	if(userPhone.trim() != ""){
		formData.append('phone_number', userPhone);
    }
    if (typeof userImage !== 'undefined'){
      formData.append('profile_image', userImage);
	}
	

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}
</script>

@endSection