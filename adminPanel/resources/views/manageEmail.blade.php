@extends('master')

@section('manageEmails')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Send Email
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Send Email</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Send Email</h3>
        </div>
        <div class="box-body">
        	<div class="box">
				<label>To</label>
				<div class="form-group" id="contactList">
					<select class="form-control select2" multiple="multiple" data-placeholder="Select Contacts"
                        style="width: 100%;" id="selectContact">

                	</select>
                </div>
                <div class="form-group">
                    <label>Subject</label>
                    <input type="text" class="form-control" placeholder="Email Subject" id="emailSubject">
                  </div>
				<div class="form-group">
                  <label>Email Body</label>
                  <textarea class="form-control" rows="10" id="emailTextArea"></textarea>
				</div>
				<div class="form-group" style="text-align:right;">
					<button type="submit" class="btn btn-primary" onClick="submitAction()">Submit</button>
                </div>
        	</div>
          <!-- /.box -->
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>

$(document).ready(function(){
  clearAllField();
  getUserList();
});

function submitAction(){

	let contactList = $('#selectContact option:selected')
                .toArray().map(item => item.value);
	if(contactList.length == 0){
		swal({
        	title: "Alert!",
        	text: 'Please enter contacts from contact list to send message.',
        	icon: "warning"
      	});
		return;
	}
	let contactListString = contactList.join(",");
	let emailBody = CKEDITOR.instances.emailTextArea.getData()
    let emailSubject = document.getElementById('emailSubject').value;
	if(emailBody.length == 0){
		swal({
        	title: "Alert!",
        	text: 'Please enter email body to compose the email.',
        	icon: "warning"
      	});
		return;
	}
    if(emailSubject.trim() == ""){
		swal({
        	title: "Alert!",
        	text: 'Please enter email subject to compose the email.',
        	icon: "warning"
      	});
		return;
	}
	sendEmails(contactListString, emailBody, emailSubject);
}


function clearAllField(){
  $("#selectContact option").prop('selected',false);
  document.getElementById("emailTextArea").value = "";
  document.getElementById("emailSubject").value = "";
}


// API Call handles here
async function getUserList(){

    const endPoint = '{{env("APP_BASE_URL", "")}}/getAllUsers';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          var index = 0;
		  let contactList = document.getElementById("selectContact");
          $.each(resultJSON.data, function(key, value){
  			let option = document.createElement("option");
  			option.text = value.name + " (" + value.email + ")";
			option.value = value.email
			contactList.options.add(option, index);
            index += 1;
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
		console.log(error);
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }

}


async function sendEmails(contactList, emailBody ,emailSubject){

    const endPoint = '{{env("APP_BASE_URL", "")}}/sendMarketingEmail';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('emailAddress', contactList);
    formData.append('emailSubject  ', emailSubject);
    formData.append('emailBody  ', emailBody);

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: "Email sent successfully.",
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          }).then(function(){
            clearAllField();
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      }).then(function(){
        clearAllField();
      });
    }

}


async function deleteCategory(categoryId){
    const endPoint = '{{env("APP_BASE_URL", "")}}/deleteCategory';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('category_id', categoryId);
    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}


async function getCategoryDetails(categoryId){
    const endPoint = '{{env("APP_BASE_URL", "")}}/getCategoryDetails';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('category_id', categoryId);
    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          document.getElementById("categoryName").value = resultJSON.data.category_name;
          document.getElementById("categoryStatus").value = resultJSON.data.category_status;
          document.getElementById("categoryImage").src = resultJSON.data.category_image;
          document.getElementById("categoryImage").style.display = 'block';
          document.getElementById("addCategoryLabel").innerHTML  = 'Edit Category';
          document.getElementById("saveButton").onclick = function(){
            updateCategoryDetails(categoryId);
          }

        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}


async function updateCategoryDetails(categoryId){

    let categoryName = document.getElementById("categoryName").value;
    let categoryStatus = document.getElementById("categoryStatus").value;
    let categoryImage = document.getElementById("fileToUpload").files[0];
    if(categoryName.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter category name.',
        icon: "warning"
      });
      return;
    }
    const endPoint = '{{env("APP_BASE_URL", "")}}/updateCategory';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('category_id', categoryId);
    formData.append('category_name', categoryName);
    formData.append('category_status', categoryStatus);
    if (typeof categoryImage !== 'undefined'){
      formData.append('category_image', categoryImage);
    }

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}
</script>

@endSection
