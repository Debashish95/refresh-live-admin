@extends('master')

@section('manageProducts')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Products
        <small>Manage the product list</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Manage Product</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Product List</h3>
        </div>
        <div class="box-body">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">&nbsp;</h3>
              <button class="btn btn-block btn-primary btn-flat" style="width: 150px;"
              data-toggle="modal" data-target="#modal-default"
              onclick="clearAllField()">Add Product</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Product Name</th>
                  <th>Product Image</th>
                  <th>Product Info</th>
                  <th>Product Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          &nbsp;
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addCategoryLabel">Add Product</h4>
              </div>
              <div class="modal-body">
              <form class="form-horizontal">
                <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Product Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Category Name" id="categoryName">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Product Status</label>
                  <div class="col-sm-10">
                    <select class="form-control" id='categoryStatus'>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Product Image</label>
                  <div class="col-sm-10">
                  <img src="" style="width: 150px; height: 150px" id="categoryImage" name="categoryImage">
                  <input type="file" id="fileToUpload" accept="image/*" onchange="changeImage()">
                  </div>
                </div>
                </div>
              </form>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                id="closeButton" onclick="clearAllField()">Close</button>
                <button type="button" class="btn btn-primary" id="saveButton" onclick="createCategory()">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>

$(document).ready(function(){
  clearAllField();
  getCategoryList();
});

function changeImage(){
    var categoryImage = document.getElementById("fileToUpload").files[0];
    if (typeof categoryImage !== 'undefined'){
        var fileExtension = categoryImage.type.split('/').pop().toLowerCase();
        if (fileExtension != "jpeg" && fileExtension != "jpg" && fileExtension != "png") {
            alert('Please select a valid image file');
            document.getElementById("fileToUpload").value = '';
            document.getElementById("categoryImage").style.display = 'none';
            return;
        }
        else if (categoryImage.size > 2048000) {
            alert("Max image size is 2MB only");
            document.getElementById("fileToUpload").value = '';
            document.getElementById("categoryImage").style.display = 'none';
            return;
        }
        else{
            document.getElementById("categoryImage").style.display = 'block';
            document.getElementById("categoryImage").src =  window.URL.createObjectURL(categoryImage);
        }
    }
}


function clearAllField(){
  $("#categoryStatus option").prop('selected',false);
  document.getElementById("categoryName").value = "";
  document.getElementById("categoryName").required = false;
  document.getElementById("fileToUpload").value = '';
  document.getElementById("categoryImage").style.display = 'none';
  document.getElementById("addCategoryLabel").innerHTML  = 'Add Category';

  document.getElementById("closeButton").disabled = false;
  document.getElementById("saveButton").disabled = false
  document.getElementById("saveButton").onclick = function(){
    createCategory();
  }
}

function deleteAction(categoryId) {
  swal({
    title: "Alert!",
    text: "Are you sure you want to delete the category?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      deleteCategory(categoryId);
    }
  });
}


// API Call handles here
async function getCategoryList(){
    let categoryStatusActive = `<label class="label-success">Active</label>`;
    let categoryStatusDeactive = `<label class="label-danger">Deactive</label>`;
    const endPoint = '{{env("APP_BASE_URL", "")}}/getProductList';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          var index = 0;
          var categoryTable = $('#example1').DataTable();
          categoryTable.clear();
          $.each(resultJSON.data, function(key, value){
            index += 1;

            var productType = "";
            if(value.product_type == "0"){
                productType = 'VEG'
            }
            else if(value.product_type == "1"){
                productType = 'EGG'
            }
            else if(value.product_type == "2"){
                productType = 'NON-VEG'
            }

            let categoryImage = `<img src="${value.product_image}" style="width: 50px; height: 50px;" alt= "...">`;
            let actionButton = `<div class="btn-group">
                  <button type="button" class="btn btn-default btn-flat">Action</button>
                  <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">

                    <li><a onclick="getCategoryDetails(${value.id})" data-toggle="modal" data-target="#modal-default">Edit</a></li>
                    <li><a onclick="deleteAction(${value.id})">Delete</a></li>
                  </ul>
                </div>`;
            let productInfo = `
                <label>Price: ₹${value.product_price}</label><br>
                <label>Type: ${productType}</label>
            `;
                //<li><a onclick="deactivateAction(${value})">${(value.category_status == "1" ? "Deactive" : "Active")}</a></li>
            let categoryStatus = (value.product_status == "1" ? categoryStatusActive : categoryStatusDeactive);
            categoryTable.row.add([
              index, value.product_name, categoryImage, productInfo, categoryStatus, actionButton
            ]).draw();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }

}


async function createCategory(){
    let categoryName = document.getElementById("categoryName").value;
    let categoryStatus = document.getElementById("categoryStatus").value;
    let categoryImage = document.getElementById("fileToUpload").files[0];

    if(categoryName.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter category name.',
        icon: "warning"
      });
      return;
    }

    const endPoint = '{{env("APP_BASE_URL", "")}}/createCategory';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('category_name', categoryName);
    formData.append('category_status', categoryStatus);
    if (typeof categoryImage !== 'undefined'){
      formData.append('category_image', categoryImage);
    }


    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          }).then(function(){
            clearAllField();
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      }).then(function(){
        clearAllField();
      });
    }

}


async function deleteCategory(categoryId){
    const endPoint = '{{env("APP_BASE_URL", "")}}/deleteCategory';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('category_id', categoryId);
    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}


async function getCategoryDetails(categoryId){
    const endPoint = '{{env("APP_BASE_URL", "")}}/getProductDetails';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('product_id', categoryId);
    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          document.getElementById("categoryName").value = resultJSON.data.product_name;
          document.getElementById("categoryStatus").value = resultJSON.data.product_status;
          document.getElementById("categoryImage").src = resultJSON.data.product_image;
          document.getElementById("categoryImage").style.display = 'block';
          document.getElementById("addCategoryLabel").innerHTML  = 'Edit Product';
          document.getElementById("saveButton").onclick = function(){
            updateCategoryDetails(categoryId);
          }

        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}


async function updateCategoryDetails(categoryId){

    let categoryName = document.getElementById("categoryName").value;
    let categoryStatus = document.getElementById("categoryStatus").value;
    let categoryImage = document.getElementById("fileToUpload").files[0];
    if(categoryName.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter category name.',
        icon: "warning"
      });
      return;
    }
    const endPoint = '{{env("APP_BASE_URL", "")}}/updateProductDetails';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('product_id', categoryId);
    formData.append('product_name', categoryName);
    formData.append('product_status', categoryStatus);
    if (typeof categoryImage !== 'undefined'){
      formData.append('product_image', categoryImage);
    }

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}
</script>

@endSection
