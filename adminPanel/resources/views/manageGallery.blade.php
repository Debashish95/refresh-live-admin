@extends('master')

@section('manageGallery')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Gallery
        <small>Manage the image list</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Manage Gallery</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Gallery</h3>
        </div>
        <div class="box-body">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">&nbsp;</h3>
              <button class="btn btn-block btn-primary btn-flat" style="width: 150px;"
              data-toggle="modal" data-target="#modal-default"
              onclick="clearAllField()">Add Image</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Image Name</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          &nbsp;
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addCategoryLabel">Add Image</h4>
              </div>
              <div class="modal-body">
              <form class="form-horizontal">
                <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Image Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Image Name" id="categoryName">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Upload Image</label>
                  <div class="col-sm-10">
                  <img src="" style="width: 150px; height: 150px" id="categoryImage" name="categoryImage">
                  <input type="file" id="fileToUpload" accept="image/*" onchange="changeImage()">
                  </div>
                </div>
                </div>
              </form>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                id="closeButton" onclick="clearAllField()">Close</button>
                <button type="button" class="btn btn-primary" id="saveButton" onclick="createCategory()">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>

$(document).ready(function(){
  clearAllField();
  getImageList();
});

function changeImage(){
    var categoryImage = document.getElementById("fileToUpload").files[0];
    if (typeof categoryImage !== 'undefined'){
        var fileExtension = categoryImage.type.split('/').pop().toLowerCase();
        if (fileExtension != "jpeg" && fileExtension != "jpg" && fileExtension != "png") {
            alert('Please select a valid image file');
            document.getElementById("fileToUpload").value = '';
            document.getElementById("categoryImage").style.display = 'none';
            return;
        }
        else if (categoryImage.size > 2048000) {
            alert("Max image size is 2MB only");
            document.getElementById("fileToUpload").value = '';
            document.getElementById("categoryImage").style.display = 'none';
            return;
        }
        else{
            document.getElementById("categoryImage").style.display = 'block';
            document.getElementById("categoryImage").src =  window.URL.createObjectURL(categoryImage);
        }
    }
}


function clearAllField(){
  $("#categoryStatus option").prop('selected',false);
  document.getElementById("categoryName").value = "";
  document.getElementById("categoryName").required = false;
  document.getElementById("fileToUpload").value = '';
  document.getElementById("categoryImage").style.display = 'none';
  document.getElementById("addCategoryLabel").innerHTML  = 'Add Image';

  document.getElementById("closeButton").disabled = false;
  document.getElementById("saveButton").disabled = false
  document.getElementById("saveButton").onclick = function(){
    createCategory();
  }
}

function deleteAction(categoryId) {
  swal({
    title: "Alert!",
    text: "Are you sure you want to delete the image?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      deleteCategory(categoryId);
    }
  });
}


// API Call handles here
async function getImageList(){
    let categoryStatusActive = `<label class="label-success">Active</label>`;
    let categoryStatusDeactive = `<label class="label-danger">Deactive</label>`;
    const endPoint = '{{env("APP_BASE_URL", "")}}/getImageList';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          var index = 0;
          var categoryTable = $('#example1').DataTable();
          categoryTable.clear();
          $.each(resultJSON.data, function(key, value){
            index += 1;

            let categoryImage = `<img src="${value.thumbnail_image}" style="width: 50px; height: 50px;" alt= "...">`;
            let actionButton = `<div class="btn-group">
                  <button type="button" class="btn btn-default btn-flat">Action</button>
                  <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a onclick="deleteAction(${value.id})">Delete</a></li>
                  </ul>
                </div>`;
            categoryTable.row.add([
              index, value.file_name, categoryImage, actionButton
            ]).draw();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }

}


async function createCategory(){
    let categoryName = document.getElementById("categoryName").value;
    let categoryImage = document.getElementById("fileToUpload").files[0];

    if(categoryName.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter image name.',
        icon: "warning"
      });
      return;
    }

    const endPoint = '{{env("APP_BASE_URL", "")}}/createImage';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('file_name', categoryName);
    if (typeof categoryImage !== 'undefined'){
      formData.append('original_file', categoryImage);
    }


    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          }).then(function(){
            clearAllField();
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      }).then(function(){
        clearAllField();
      });
    }

}


async function deleteCategory(categoryId){
    const endPoint = '{{env("APP_BASE_URL", "")}}/deleteImage';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('image_id', categoryId);
    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}

</script>

@endSection
