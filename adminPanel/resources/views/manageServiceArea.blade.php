@extends('master')

@section('manageServiceArea')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage Service Area
        <small>Manage the service area list</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Manage Service Area</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Manage Service Area List</h3>
        </div>
        <div class="box-body">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">&nbsp;</h3>
              <button class="btn btn-block btn-primary btn-flat" style="width: 150px;"
              data-toggle="modal" data-target="#modal-default"
              onclick="clearAllField()">Add Service Area</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Area Name</th>
                  <th>Area Info</th>
                  <th>Area Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          &nbsp;
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addServiceLabel">Add Service Area</h4>
              </div>
              <div class="modal-body">
              <form class="form-horizontal">
                <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Area Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Area Name" id="areaName">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">PIN Code</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Area PIN Code" id="areaPIN">
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Shipping Method Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" placeholder="Shipping Method Name" id="shippingMethodName">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Shipping Charge</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" placeholder="Shipping Charge" id="shippingCharge">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Average Shipping Time</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" placeholder="Average Shipping Time" id="averageShippingTime">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Minimum Order Amount</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" placeholder="Minimum Order Amount For Free Shipping" id="minimumOrder">
                    </div>
                  </div>
                </div>
              </form>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"
                id="closeButton" onclick="clearAllField()">Close</button>
                <button type="button" class="btn btn-primary" id="saveButton" onclick="createCategory()">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>

$(document).ready(function(){
  clearAllField();
  getServiceAreaList();
});


function clearAllField(){

  document.getElementById("areaName").value = "";
  document.getElementById("areaName").required = false;

  document.getElementById("areaPIN").value = "";
  document.getElementById("areaPIN").required = false;

  document.getElementById("shippingMethodName").value = "";
  document.getElementById("shippingMethodName").required = false;

  document.getElementById("shippingCharge").value = "";
  document.getElementById("shippingCharge").required = false;

  document.getElementById("averageShippingTime").value = "";
  document.getElementById("averageShippingTime").required = false;

  document.getElementById("minimumOrder").value = "";
  document.getElementById("minimumOrder").required = false;


  document.getElementById("addServiceLabel").innerHTML  = 'Add Category';

  document.getElementById("closeButton").disabled = false;
  document.getElementById("saveButton").disabled = false
  document.getElementById("saveButton").onclick = function(){
    createServiceArea();
  }
}

function deleteAction(serviceId) {
  swal({
    title: "Alert!",
    text: "Are you sure you want to delete the service area?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
        deleteServiceArea(serviceId);
    }
  });
}


// API Call handles here
async function getServiceAreaList(){
    let areaStatusActive = `<label class="label-success">Active</label>`;
    let areaStatusDeactive = `<label class="label-danger">Deactive</label>`;
    const endPoint = '{{env("APP_BASE_URL", "")}}/getServiceAreaList';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);

        if(resultJSON.code == 200){
          var index = 0;
          var serviceTable = $('#example1').DataTable();
          serviceTable.clear();
          $.each(resultJSON.data, function(key, value){
            index += 1;

            let actionButton = `<div class="btn-group">
                  <button type="button" class="btn btn-default btn-flat">Action</button>
                  <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">

                    <li><a onclick="getServiceAreaDetails(${value.id})" data-toggle="modal" data-target="#modal-default">Edit</a></li>
                    <li><a onclick="deleteAction(${value.id})">Delete</a></li>
                  </ul>
                </div>`;

            let serviceInfo = `
                <label>Locality: ${value.locality}</label><br>
                <label>City: ${value.city}</label><br>
                <label>PIN Code: ${value.pin_code}</label><br>
                <label>Shipping Charge: ₹${value.shipping_charge}</label><br>
                <label>Shipping Method: ${value.shipping_method}</label><br>
                <label>Avegare Shipping Time: ${value.average_time} mins</label><br>
                <label>Minimum Order Amount: ₹${value.minimum_order_amount}</label>
            `;
            serviceTable.row.add([
              index, value.locality, serviceInfo, areaStatusActive, actionButton
            ]).draw();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }

}


async function createServiceArea(){
    let locality = document.getElementById("areaName").value;
    let pinCode = document.getElementById("areaPIN").value;
    let shippingCharge = document.getElementById("shippingCharge").value;
    let shippingMethod = document.getElementById("shippingMethodName").value;
    let averageTime = document.getElementById("averageShippingTime").value;
    let minimumOrder = document.getElementById("minimumOrder").value;
    if(locality.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter locality name.',
        icon: "warning"
      });
      return;
    }
    if(pinCode.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter locality PIN Code.',
        icon: "warning"
      });
      return;
    }
    if((pinCode.length != 6) || isNaN(pinCode)){
      swal({
        title: "Alert!",
        text: 'Invalid PIN Code.',
        icon: "warning"
      });
      return;
    }
    if(shippingMethod.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter shipping method.',
        icon: "warning"
      });
      return;
    }
    if(shippingCharge.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter service charge.',
        icon: "warning"
      });
      return;
    }
    if(isNaN(shippingCharge)){
      swal({
        title: "Alert!",
        text: 'Invalid Shipping Charge.',
        icon: "warning"
      });
      return;
    }

    if(averageTime.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter average delivery time.',
        icon: "warning"
      });
      return;
    }
    if(isNaN(averageTime)){
      swal({
        title: "Alert!",
        text: 'Invalid average delivery time.',
        icon: "warning"
      });
      return;
    }

    if(minimumOrder.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter minimum order value for free shipping.',
        icon: "warning"
      });
      return;
    }
    if(isNaN(minimumOrder)){
      swal({
        title: "Alert!",
        text: 'Invalid Minimum Oerder Price.',
        icon: "warning"
      });
      return;
    }


    const endPoint = '{{env("APP_BASE_URL", "")}}/createServiceArea';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('locality', locality);
    formData.append('pin_code', pinCode);
    formData.append('shipping_charge', shippingCharge);
    formData.append('shipping_method', shippingMethod);
    formData.append('average_time', averageTime);
    formData.append('minimum_order_amount', minimumOrder);
    formData.append('city', 'Bhubaneswar');
    formData.append('state', 'Odisha');
    formData.append('country', 'India');

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          }).then(function(){
            clearAllField();
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      }).then(function(){
        clearAllField();
      });
    }

}


async function deleteServiceArea(serviceId){
    const endPoint = '{{env("APP_BASE_URL", "")}}/deleteServiceArea';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('service_area_id', serviceId);
    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}


async function getServiceAreaDetails(serviceId){
    const endPoint = '{{env("APP_BASE_URL", "")}}/getServiceAreaList';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('service_id', serviceId);
    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          document.getElementById("areaName").value = resultJSON.data.locality;
          document.getElementById("areaPIN").value = resultJSON.data.pin_code;
          document.getElementById("shippingMethodName").value = resultJSON.data.shipping_method;
          document.getElementById("shippingCharge").value = resultJSON.data.shipping_charge;
          document.getElementById("averageShippingTime").value = resultJSON.data.average_time;
          document.getElementById("minimumOrder").value = resultJSON.data.minimum_order_amount;
          document.getElementById("addServiceLabel").innerHTML  = 'Edit Service Area';
          document.getElementById("saveButton").onclick = function(){
            updateServiceAreaDetails(serviceId);
          }

        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}


async function updateServiceAreaDetails(serviceId){

    let locality = document.getElementById("areaName").value;
    let pinCode = document.getElementById("areaPIN").value;
    let shippingCharge = document.getElementById("shippingCharge").value;
    let shippingMethod = document.getElementById("shippingMethodName").value;
    let averageTime = document.getElementById("averageShippingTime").value;
    let minimumOrder = document.getElementById("minimumOrder").value;
    if(locality.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter locality name.',
        icon: "warning"
      });
      return;
    }
    if(pinCode.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter locality PIN Code.',
        icon: "warning"
      });
      return;
    }
    if((pinCode.length != 6) || isNaN(pinCode)){
      swal({
        title: "Alert!",
        text: 'Invalid PIN Code.',
        icon: "warning"
      });
      return;
    }
    if(shippingMethod.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter shipping method.',
        icon: "warning"
      });
      return;
    }
    if(shippingCharge.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter service charge.',
        icon: "warning"
      });
      return;
    }
    if(isNaN(shippingCharge)){
      swal({
        title: "Alert!",
        text: 'Invalid Shipping Charge.',
        icon: "warning"
      });
      return;
    }

    if(averageTime.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter average delivery time.',
        icon: "warning"
      });
      return;
    }
    if(isNaN(averageTime)){
      swal({
        title: "Alert!",
        text: 'Invalid average delivery time.',
        icon: "warning"
      });
      return;
    }

    if(minimumOrder.trim() == ""){
      swal({
        title: "Alert!",
        text: 'Please enter minimum order value for free shipping.',
        icon: "warning"
      });
      return;
    }
    if(isNaN(minimumOrder)){
      swal({
        title: "Alert!",
        text: 'Invalid Minimum Oerder Price.',
        icon: "warning"
      });
      return;
    }


    const endPoint = '{{env("APP_BASE_URL", "")}}/updateServiceArea';
    const formData = new FormData();
    formData.append('authToken', '{{env("APP_TOKEN", "")}}');
    formData.append('locality', locality);
    formData.append('pin_code', pinCode);
    formData.append('shipping_charge', shippingCharge);
    formData.append('shipping_method', shippingMethod);
    formData.append('average_time', averageTime);
    formData.append('minimum_order_amount', minimumOrder);
    formData.append('service_area_id', serviceId);

    try {
        const response = await fetch(endPoint, {
        method: 'POST',
        body: formData
    });
        const result = await response.json();
        var resultJSON = JSON.stringify(result);
        resultJSON = JSON.parse(resultJSON);
        if(resultJSON.code == 200){
          swal({
            title: "Success",
            text: resultJSON.message,
            icon: "success"
          }).then(function(){
            location.reload();
          });
        }
        else{
          swal({
            title: "Error",
            text: resultJSON.message,
            icon: "error"
          });
        }
    } catch (error) {
      swal({
        title: "Error",
        text: "Something went wrong!",
        icon: "error"
      });
    }
}
</script>

@endSection
