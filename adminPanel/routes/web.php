<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->middleware('auth');

Route::get('/master', function () {
    return view('dashboard');
})->middleware('auth');
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware('auth');

Route::get('/manageCategory', function () {
    return view('manageCategory');
})->middleware('auth');

Auth::routes();
Route::get('/logout', function(){
    Auth::logout();
    return redirect('/login');
});

Route::get('/home', function () {
    return view('dashboard');
})->middleware('auth');

Route::get('/manageUser', function () {
    return view('manageUsers');
})->middleware('auth');

Route::get('/sendSMS', function () {
    return view('manageSMS');
})->middleware('auth');

Route::get('/sendEmail', function () {
    return view('manageEmail');
})->middleware('auth');


Route::get('/manageServiceArea', function () {
    return view('manageServiceArea');
})->middleware('auth');

Route::get('/manageProducts', function () {
    return view('manageProducts');
})->middleware('auth');

Route::get('/manageGallery', function () {
    return view('manageGallery');
})->middleware('auth');
